import React, { Component } from 'react';
import { Container, Header, Content, Form, Item, Input, Label,Radio,Text,Button,View,Right,Footer} from 'native-base';
import styles from "./style.js";
import { ImageBackground ,Image,TouchableOpacity,Alert } from "react-native";
const peoplebg =require("./assets/people.png");
import axios from "axios";
import {API ,APIV ,Login}from"./config";
import {  Dimensions } from 'react-native'
let ScreenHeight = Dimensions.get("window").height;

export default class FloatingLabelExample extends Component {
  state={
    email:"",
    password:"",
    username:"",
    token:"",
    name:"",
    gender:"",
    avatar:"",
    country_en:"",
    country_id:"",
    isReady:false
  }

   Login =async()=>{
    this.setState({isReady:false});
    //here you are login request 
    await axios
      .post( API + APIV + Login , {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        email: this.state.email,
        password: this.state.password
      })
      .then(result => {

        console.log("User has been set");
      })
      .catch(function (error) {
        if (error.response) {
          //error handling
          alert(error.response.data.error);
        }
      });
    this.setState({isReady:true});

  }

  render() {
    return (
        <Container>
      <Content padder contentContainerStyle={{height:ScreenHeight ,paddingHorizontal:10 ,}} >
        <View style={{flex:0.2,justifyContent:"flex-end"}} ><Text style={styles.TitleText} >Welcome back !</Text></View>
          <Form style={{flex:0.5 ,paddingHorizontal:10 }}>
              <Item floatingLabel>
                  <Label>E-mail</Label>
                  <Input 
                  onChangeText={text => this.validate_EM(text, "Email")}
                  value={this.state.email} />
                </Item>
                <Item floatingLabel >
                  <Label>Password</Label>
                  <Input 
                  onChangeText={text => this.validate_PS(text, "password")}
                  value={this.state.password} />
                </Item>
                <View style={styles.ViewRow}>
                <View style={styles.ViewRow}>
                  <Radio selected={true} />
                  <Text> Keep me logged in</Text>
                </View>
                <Right><Text style={styles.TextBrand}>Forget Password?</Text></Right>
                </View>

                <View><Button   onPress={() => {this.Login();}}rounded block><Text>Login</Text></Button></View>
                <View style={styles.ViewCenter}><Text>New user? </Text>
                <View><TouchableOpacity  onPress ={()=>{this.props.navigation.navigate("Sign")}}  ><Text style={styles.TextBrand}> Sign Up</Text></TouchableOpacity ></View>            
                </View>
          </Form>

          <View style={{flex:0.3 ,justfiyContent:"flex-end" ,alignItems: 'center',}}><Image source={peoplebg} /></View>
          </Content>
      </Container>
    );
  }

  validate_PS = (text, type) => {
    var self = this;

    if ((type = "password")) {
      self.setState({
        password: text
      });
      if (text.length < 5) {
        self.setState({ PasswordError: true });
      } else {
        self.setState({ PasswordError: false });

      }
    }
  };

  validate_EM = (text, type) => {
    var self = this;

    if (type == "Email") {
      self.setState({
        email: text
      });
      if (text.length < 5) {
        self.setState({ EmailError: true });
      } else {
        self.setState({ EmailError: false });

      }
    }
  }
}