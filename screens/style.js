import { whileStatement } from "@babel/types";
const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth =Dimensions.get("window").width;

export default {
    TitleText: {
        fontSize:30
      },
      ViewCenter:{
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5,
    
      },
      ViewRow:{
        flex:1,
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 5,
        alignItems: 'center',
    
      },
      TextBrand:{
        color:"#F22E63"

      },
      PeopleImage:{
        alignself:"center",
        
      }
    }