import React, { Component } from 'react';
import { Container, Header, Content, ListItem, Text, Radio, Right, Left, View } from 'native-base';
import styles from'../style';

export default class RadioButtonExample extends Component {
    constructor(props) {
        super(props);
        this.state = {
            male: false,
            female: false,
        };

    }
    componentWillMount=()=>{
        if(this.props.gender=="m"){this.setState({
            female: false,
            male: true
        });}else if(this.props.gender=="f"){
            this.setState({
                female: true,
                male: false
            });

        }
    }
     togglefemale() {
        this.setState({
            female: true,
            male: false
        });
         this.props.genderChange('f');

    }
    togglemale() {
        this.setState({
            male: true,
            female: false
        });
        this.props.genderChange('m');

    }
    render() {
        return (
        <View
        style={styles.ViewCenter}>

                <Radio selected={false}
                color={this.props.GenderError ? "red": "#F22E63"}
                selectedColor="#F22E63"
                style={{
                    marginRight:5,
                }}
                    selected={this.state.male}
                    onPress={() => this.togglemale()}
/>
                <Text styles={styles.colorBrand} >Man</Text>

                <Radio selected={true}
                color={this.props.GenderError ? "red": "#F22E63"}
                selectedColor="#F22E63"
                style={{
                    marginRight: 5 ,
                    marginLeft :15
                }}

                    selected={this.state.female}
                    onPress={() => this.togglefemale()} />
                <Text styles={styles.colorBrand}>Woman</Text>


        </View>

        );
    }
}