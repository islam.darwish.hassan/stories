import React, { Component } from 'react';
import {  StyleSheet  } from 'react-native';
import { View, Text, Button, Image, Thumbnail} from 'native-base';
import * as ImagePicker from 'expo-image-picker';
const cover = require("../assets/profile.png");


export default class App extends Component {
  state = {
    pickerResult: cover,
  };
componentWillMount=()=>{
if(this.props.avatar!=""&&this.props.avatar!=null){
  this.setState({pickerResult:this.props.avatar})
}
}
  _pickImg = async () => {
    let pickerResult = await ImagePicker.launchImageLibraryAsync({
      base64: true,
      allowsEditing: false,
      mediaTypes : 'Images',
      aspect: [4, 3],
      quality:0.5
    });


    if (!pickerResult.cancelled) {
      this.setState({
        pickerResult,
      });
    let imageUri = pickerResult    
    if(imageUri!=cover && imageUri!=null && imageUri!="")
     {imageUri= `data:image/jpg;base64,${pickerResult.base64}`   
    }else{
      imageUri=""
    }
    console.log({ uri: imageUri.slice(0, 100) });
    this.props.imgChange(imageUri);
  }else{

  }
  };

  render() {
             let { pickerResult } = this.state;


             // <Button onPress={this._pickImg} title="Open Picker" />
             return (
               <Button
                 style={{
                   margin: 20
                 }}
                 transparent onPress={this._pickImg}>

                   <Thumbnail  source={!this.props.avatar? pickerResult: {uri:this.props.avatar}}
                  />
                 <Text>
                   Upload Your Profile Picture *
                </Text>
               </Button>
             );
           }
}

