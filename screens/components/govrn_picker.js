import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Picker,
  Text,
  View
} from "native-base";
import {APIV,API,Govrn_Query}from "../config"
import styles from "../style";

import axios from "axios";
export default class PickerInputExample extends Component {
  state = {
    governrates: [],
    data: [],
    selected2: '',
  };
  componentDidMount() {
    axios.get(API + APIV + Govrn_Query).then(res => {
     this.setState({
        governrates: res.data.data.countries
      });
    //  console.log(this.state.governrates);
    });
  }

  onValueChange2(value: string) {
    if (value == "0") {
      this.setState({
        selected3: "0",
        cities: []
      });
    }

    //console.log(value);
    this.setState({
      selected2: value
    });
    this.props.governChange(value.toString());


  }


  render() {
    return (
      <Content>
        <Item  picker
         //error={this.props.VGovern}

                    style={styles.FormItem}>
          <Picker
            mode="dropdown"
            style={{ width: undefined }}
            placeholder="test"
            placeholderStyle={{
              color: "#555"
            }}
            placeholderIconColor="#007aff"
            selectedValue={this.state.selected2}
            onValueChange={this.onValueChange2.bind(this)}
          >
            <Picker.Item label="Governrate*" value="0" />
            {this.state.governrates.map(governrate => (
              <Picker.Item
                label={governrate.en_name}
                key={governrate.id}
                value={governrate.id}
              />
            ))}
          </Picker>
        </Item>
        
      </Content>
    );
  }
}
