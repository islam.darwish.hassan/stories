import React from 'react';
import axios from "axios";
import { Container, Content, Text,View ,Header,Item, Thumbnail } from 'native-base';

const API ="http://10.1.156.90:80/siliconarena/public/api/";
const APIV="v1/";
const Link ="bloggers";

export default class root extends React.Component {
state={
  users:[],
  isReady:false

};

componentWillMount=()=>{
this.Get_Posts();
}

Get_Posts = async ()=>{
this.setState({isReady:false});
await axios
.get(API+APIV+Link)
.then(result => {
  this.setState({users:result.data.data.bloggers});
  console.log("Bloggers has been set");
})
.catch(error => {
  console.log(error);
});
this.setState({isReady:true});


}
    render() {
      
      return (
        this.state.isReady !=true ?  
        <Text>App is not ready </Text>
        :(
          <Container>
            <Header><Text>Application Name</Text></Header>
            <Content padder>
          {this.state.users.map((value,index)=>{
          return(
            <View  key={index} >
            <Item><Text>{value.name}</Text></Item>
            <Text>{value.email}</Text>
            <Thumbnail large source={{uri:value.avatar}}/>
            </View>
          );
          }
          )}
          </Content>
          </Container>
        
      )
      ) }
  }
