import React, { Component } from 'react';
import { Container, Header, Content, Form, Item, Input, Label, Radio, Text, Button, View, Right, Footer } from 'native-base';
import styles from "./style.js";
import { ImageBackground, Image, TouchableOpacity } from "react-native";
const peoplebg = require("./assets/people.png");
import axios from "axios";
import { API, APIV, Sign } from "./config";
import Gender from "./components/RadioBtn";
import Country from "./components/govrn_picker";
import Avatar from "./components/imagepickerbase64";
export default class SignUp extends Component {
  //states
  state = {
    email: "",
    password: "",
    username: "",
    token: "",
    name: "",
    gender: "",
    avatar: "",
    country_en: "",
    country_id: "",
    isReady: false
  }
  //requests
  Sign = async () => {
    this.setState({ isReady: false });
    //here you are login request 
    await axios
      .post(API + APIV + Sign, {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        username: this.state.username,
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        password_confirmation: this.state.password_confirmation,
        gender: this.state.gender,
        country_id: this.state.country_id,
        avatar: this.state.avatar
      })
      .then(result => {

        console.log("User has been set");
      })
      .catch(function (error) {
        if (error.response) {
          //error handling
          alert(error.response.data.error);
        }
      });
    this.setState({ isReady: true });

  }
  //render
  render() {
    return (
      <Container>
        <Content style={{ flex: 1 }} padder >
          <View style={{ flex: 1, justifyContent: "center" }}><Text style={styles.TitleText} >Join with us !</Text></View>
          <Form style={{ flex: 1 }}>
            <View><Item floatingLabel>
              <Label>Username</Label>
              <Input
                onChangeText={text => this.validate_UNa(text, "username")}
                value={this.state.username}
              />
            </Item>
              <Item floatingLabel>
                <Label>Your Name</Label>
                <Input
                  onChangeText={text => this.validate_Na(text, "name")}
                  value={this.state.name}
                />
              </Item>
              <Item floatingLabel >
                <Label>Password</Label>
                <Input
                  secureTextEntry
                  onChangeText={text => this.validate_PS(text, "password")}
                  value={this.state.password}
                />
              </Item>
              <Item floatingLabel >
                <Label>Confirm Password</Label>
                <Input
                  secureTextEntry
                  onChangeText={text => this.validate_PSC(text, "password_confirm")}
                  value={this.state.password_confirmation}
                />
              </Item>
              <Item floatingLabel >
                <Label>E-mail</Label>
                <Input
                  onChangeText={text => this.validate_EM(text, "email")}
                  value={this.state.email}
                />
              </Item>

                <Country
                  VGovern={this.state.VGovern}
                  governChange={this.onChangegovern.bind(this)}
                />
              <Gender GenderError={this.state.GenderError} genderChange={this.onChangeGender.bind(this)} />
              <Avatar imgChange={this.onImgchange.bind(this)} />

              <Button onPress={() => this.Sign()} rounded block><Text>Sign Up </Text></Button>
              <View style={styles.ViewCenter}><Text note>Are you already one of family ? </Text>
                <TouchableOpacity onPress={() => { this.props.navigation.navigate("Sign") }}  ><Text style={styles.TextBrand}> Sign In</Text></TouchableOpacity >
              </View>
            </View>
          </Form>
          <View style={{ flex: 1, justfiyContent: "flex-end" }}><Image source={peoplebg} /></View>
        </Content>
      </Container>
    );
  }

  //handlers
  onChangeGender(gen) {
    var self = this;

    self.setState({ gender: gen });
    console.log(this.state.gender);

  }
  onChangegovern(gov) {
    var self = this;

    self.setState({ country_id: gov });
    //console.log(this.state.governrate_id);

  }
  onImgchange(image) {
    var self = this;

    self.setState({ avatar: image });
    //console.log(this.state.avatar);
  }


  //validation
  validate_Na = (text, type) => {
    if (type == 'name') {
      var self = this;
      self.setState({
        name: text
      });
      if (text.length < 3) {
        self.setState({ NameError: true });
      }
      else {
        self.setState({ NameError: false });

      }
    }
  }
  validate_UNa = (text, type) => {
    if (type == 'username') {
      var self = this;
      self.setState({
        username: text
      });
      if (text.length < 3) {
        self.setState({ UsernameError: true });
      }
      else {
        self.setState({ UsernameError: false });

      }
    }
  }
  validate_PS = (text, type) => {

    var self = this;

    if ((type = "password")) {
      self.setState({
        password: text
      });
      if (text.length < 5) {
        self.setState({ VPasswordError: true });
      } else {
        self.setState({ VPasswordError: false });

      }
    }
  }
  validate_EM = (text, type) => {
    var self = this;

    alph = /^[a-zA-Z]+$/
    if ((type = "email")) {
      self.setState({
        email: text
      });
      if (text.length < 5 || text.indexOf("@") === -1) {
        self.setState({ VEmailError: true });
      } else {
        self.setState({ VEmailError: false });

      }
    }
  }
  validate_PSC = (text, type) => {
    var self = this;

    if ((type = "password_confirm")) {
      self.setState({
        password_confirmation: text
      });
      if (text.length < 5) {
        self.setState({ VPasswordError: true });
      } else {
        self.setState({ VPasswordError: false });

      }
    }
  }

}