import React from 'react';
import { AppLoading } from 'expo';
import { Container, Content, Text, StyleProvider ,Button,View } from 'native-base';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import getTheme from './native-base-theme/components';
import material from './native-base-theme/variables/material';
import Root from "./screens/rootscreen.js";
import Sign from "./screens/SignUp.js";
import Login from "./screens/login.js";
import {
  createStackNavigator,
} from "react-navigation-stack";
import { createAppContainer } from 'react-navigation';

const AppNavigator = createStackNavigator(
  {
    Intro: { screen: Root },
    Login: { screen: Login  },
    Sign:{screen:Sign}
  },
  {
    initialRouteName: "Login",
    headerMode: "none"
  }
);

const AppContainer = createAppContainer(AppNavigator);
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
    this.setState({ isReady: true });
  }

  render() {
    if (!this.state.isReady) {
      return <AppLoading />;
    }

    return (
      <StyleProvider style={getTheme(material)}>
        <AppContainer/>
    </StyleProvider>
    );
  }
}
